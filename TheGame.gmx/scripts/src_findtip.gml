///findTip("x" or "y", xOnImage, yOnImage, sprite, angle);
if argument0 == "x" {
  return x +lengthdir_x(
            point_distance(sprite_get_xoffset(argument3), sprite_get_yoffset(argument3), argument1, argument2)
            ,
            point_direction(sprite_get_xoffset(argument3), sprite_get_yoffset(argument3), argument1, argument2)
            +argument4
            ) 
} else if argument0 == "y" {
  return y +lengthdir_y(
            point_distance(sprite_get_xoffset(argument3), sprite_get_yoffset(argument3) ,argument1, argument2)
            ,
            point_direction(sprite_get_xoffset(argument3), sprite_get_yoffset(argument3) , argument1, argument2)
            +argument4
            )
} else {
  return 0;
}
