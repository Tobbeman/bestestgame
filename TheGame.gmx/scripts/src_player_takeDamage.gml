///src_player_takeDamage

if(obj_player.vulnerable)
{
    obj_player.hp -= argument0;
    obj_player.vulnerable = false;
    obj_player.alarm[1] = obj_player.protectRate;
}

