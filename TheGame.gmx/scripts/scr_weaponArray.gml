/* 
Array structure:
[i,0] = name
[i,1] = sprite
[i,2] = rate of fire
[i,3] = bulletSprite
[i,4] = muzzle offset
[i,5] = unlocked 
[i,6] = special
[i,7] = speed
[i,8] = ammo
*/

for(i = 0; i < weapons; i++){
    for(j = 0; j < 9; j++){
        global.weaponArray[i,j] = 0;
    }
}

// [0] Pistol
global.weaponArray[0,0] = "Pistol"; //Name
global.weaponArray[0,1] = spr_player; //TEMP
global.weaponArray[0,2] = 10; //rate of fire
global.weaponArray[0,3] = obj_bullet;
global.weaponArray[0,4] = 20; //TEMP
global.weaponArray[0,5] = true; //Unlocked
global.weaponArray[0,6] = 0; //special
global.weaponArray[0,7] = 14; //speed
global.weaponArray[0,8] = 10000; //ammo

// [1] Machine gun
global.weaponArray[1,0] = "Machine Gun"; //Name
global.weaponArray[1,1] = spr_playerMachineGun; //TEMP
global.weaponArray[1,2] = 10; //rate of fire
global.weaponArray[1,3] = obj_bullet;
global.weaponArray[1,4] = 20; //TEMP
global.weaponArray[1,5] = true; //Unlocked
global.weaponArray[1,6] = 0; //special
global.weaponArray[1,7] = 14; //speed
global.weaponArray[1,8] = 80; //ammo

// [2] Shotgun
global.weaponArray[2,0] = "Shotgun"; //Name
global.weaponArray[2,1] = spr_playerShotgun;
global.weaponArray[2,2] = 15; //rate of fire
global.weaponArray[2,3] = obj_shotgunBullet;
global.weaponArray[2,4] = 20; //TEMP
global.weaponArray[2,5] = true; //Unlocked
global.weaponArray[2,6] = 5; //special
global.weaponArray[2,7] = 14; //speed
global.weaponArray[2,8] = 80; //ammo

// [3] Bazooka
global.weaponArray[3,0] = "Bazooka"; //Name
global.weaponArray[3,1] = spr_player; //TEMP
global.weaponArray[3,2] = 20; //rate of fire
global.weaponArray[3,3] = obj_rocket;
global.weaponArray[3,4] = 20; //TEMP
global.weaponArray[3,5] = true; //Unlocked
global.weaponArray[3,6] = 0; //special
global.weaponArray[3,7] = 20; //speed
global.weaponArray[3,8] = 80; //ammo
