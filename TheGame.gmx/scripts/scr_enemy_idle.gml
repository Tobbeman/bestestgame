///scr_enemy_idle
var dis = point_distance(x,y,obj_player.x,obj_player.y);
var dir = point_direction(x,y,obj_player.x,obj_player.y);
var rel = abs(image_angle - dir);

if(rel < viewRad || rel > 360 - viewRad)
{
    if(dis <= aggroRange && !collision_line(x,y,obj_player.x,obj_player.y,o_solid,1,1)){
        moving = true;
        rdyAlerted = false;
        state = attackState;
        instance_create(x,y,obj_sound);
        //Show enemy as alerted
        image_index = 1;
        alarm[10] = -1;
    }
}
