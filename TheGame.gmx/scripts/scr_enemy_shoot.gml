//Angle
var aim_angle=point_direction(x,y,obj_player.x,obj_player.y);
image_angle += sin(degtorad(aim_angle-image_angle))*rot_factor;
//Shooting
if(!collision_line(x,y,obj_player.x,obj_player.y,o_solid,1,1))
{  
    rdyAlerted = false;  
    myLegs.moving = false;
    shootAlarm--;
    if(shootAlarm <= 0)
    {
        audio_play_sound(weapon.sound,0,0);
        spawnx = src_findtip("x",weapon.xoff,weapon.yoff,spr_playerMachineGun,self.image_angle);
        spawny = src_findtip("y",weapon.xoff,weapon.yoff,spr_playerMachineGun,self.image_angle);
        
        if(myGun.name = "Shotgun")
        {
            scr_shotgun(spawnx,spawny,weapon.bulletSpeed,obj_player.x,obj_player.y)
        }
        else
        {
            with(instance_create(spawnx,spawny,weapon.bullets))
            {
                move_towards_point(obj_player.x,obj_player.y,other.weapon.bulletSpeed);
                image_angle = direction;
            }
        }
        shootAlarm = weapon.rateOfFire;
    }
}
else
{
    //show_message("LEAVING ATTACK, ENTERING INVEST");
    myLegs.moving = true;
    heardx = obj_player.x;
    heardy = obj_player.y;
    state = src_enemy_investigate;
}

