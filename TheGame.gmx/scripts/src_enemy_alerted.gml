///scr_enemy_idle
var dis = point_distance(x,y,obj_player.x,obj_player.y);
if(dis <= aggroRange && !collision_line(x,y,obj_player.x,obj_player.y,o_solid,1,1)){
    moving = true;
    state = attackState;
    //Show enemy as alerted
    image_index = 1;
}
else
{
    //These might need to be called from outside, so that other things might alert
    heardx = obj_player.x;
    heardy = obj_player.y;
    moving = true;
    state = src_enemy_investigate;
}
