///scr_shotgun (x,y,speed)
for(i=0;i<10;i++)
{
    with(instance_create(argument0,argument1,obj_shotgunBullet))
    {
        randomVal =  random_range(-30,30);
        move_towards_point(argument3 + randomVal,argument4 + randomVal,argument2);
    }
}
