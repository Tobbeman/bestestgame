///scr_enemy_follow
mp_potential_step(obj_player.x,obj_player.y, movementSpeed, false);

//
image_angle = point_direction(x, y, obj_player.x, obj_player.y);


//Aggro
var dis = point_distance(x,y,obj_player.x,obj_player.y);
if(dis >= aggroRange){
    if(alarm[11] == -1)
    {
        state = scr_enemy_idle;
        moving = false;
    }
}
if(dis < attackRange){
    state = attackState;
}
